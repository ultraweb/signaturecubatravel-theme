<div class="row">
    <div class="col-12 content-title text-center">
        <div class="divider">
            <span></span>
            <h3>Destinations</h3>
            <span></span>
        </div>
    </div>
</div>

<div class="row destinations-cols">
    <?php
    $qargs = array(
        'post_type'         =>  'post',
        'category_name'     =>  'destinations',
        'post_status'       =>  'publish',
        'order'             =>  'ASC',
    );

    $the_query = new WP_Query($qargs);
    ?>

    <div class="col-12">
        <div class="owl-carousel owl-theme">
            <?php if ($the_query->have_posts()): ?>
                <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
                    <div>
                        <a href="<?php the_permalink(); ?>">
                            <div class="item d-flex mb-3">
                                <?php the_post_thumbnail('full', ['class' => 'img-fluid zoom']); ?>
<!--                                <div class="content">-->
                                    <div class="item-title align-self-center text-center">
                                        <?php the_title(); ?>
                                    </div>
<!--                                </div>-->
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>


</div>