<?php get_header(); ?>

	<div class="container">

		<div class="row">
			<div class="col-xs-12 contenido-single">

				<div class="jumbotron text-center">
					<h1>404!</h1>
					<p>El recurso que estás buscando no se encuentra disponible.</p>
				</div>

			</div>
		</div>

	</div>

<?php get_footer() ?>