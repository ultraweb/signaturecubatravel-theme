<?php wp_footer(); ?>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid">
            </div>

            <div class="col-xs-12 col-md-3">
                <p>Signature Cuba Travel LLC is registered with the State of Florida as a Seller of Travel. Registration No. ST41644</p>
            </div>

            <div class="col-xs-12 col-md-5">
                <?php
                wp_nav_menu( array(
                    'theme_location'    => 'footer',
                    'depth'             => 2,
                    'container'         => 'div',
                    'menu_class'        => 'nav footer-nav',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker(),
                ) );
                ?>
            </div>

            <div class="col-xs-12 col-md-2">
                <ul class="nav justify-content-end social-links">
                    <li class="nav-item">
                        <a class="nav-link" href="https://g.page/signature-cuba-travel?share" target="_blank"><i class="mdi mdi-google-plus mdi-36px"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.facebook.com/SignatureCubaTravel/" target="_blank"><i class="mdi mdi-facebook mdi-36px"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://instagram.com/signaturecubatravel?igshid=13fctxdrh4nta" target="_blank"><i class="mdi mdi-instagram mdi-36px"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.youtube.com/channel/UCZm_syxUMxTqrxlybUtFHuw" target="_blank"><i class="mdi mdi-youtube-play mdi-36px"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!--<script src="--><?//= get_stylesheet_directory_uri(); ?><!--/js/jquery-2.2.3.min.js"></script>-->
<script src="<?= get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?= get_stylesheet_directory_uri(); ?>/js/bower_components/owlCarousel/owl.carousel.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>-->

<script>

    jQuery(document).ready(function ($) {

        //Event Scroll to Top Menu
        $(window).scroll(function() {
            if ($(".header-top").offset().top > 90) {
                $(".header-top").addClass("bg-secundary");
            } else {
                $(".header-top").removeClass("bg-secundary");
            }
        });

        //OWL Carouse
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:30,
            dots: false,
            nav:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true
                }
            }
        });

        $('.owl-prev').html('<i class="mdi mdi-chevron-left mdi-48px"></i>');
        $('.owl-next').html('<i class="mdi mdi-chevron-right mdi-48px"></i>');

        let collapses = $('.content-faqs').find('.card');
        let first = collapses.first();
        let collapse = first.find('.collapse').addClass('show');
        let icon = collapse.find('.mdi');

        icon.removeClass('mdi-chevron-down');
        icon.addClass('mdi-chevron-right');

        //Zoom Effect to items
        $('.zoom').hover(function() {
            $(this).addClass('transition');
        }, function() {
            $(this).removeClass('transition');
        });

    });


</script>

</body>
</html>