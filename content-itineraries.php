<div class="row">
    <div class="col-12 content-title text-center">
        <div class="divider">
            <span></span>
            <h3>Itineraries</h3>
            <span></span>
        </div>
    </div>
</div>

<div class="row itineraries-cols">
    <?php
    $qargs = array(
        'post_type'         =>  'post',
        'category_name'     =>  'itineraries',
        'post_status'       =>  'publish',
        'order'             =>  'ASC',
        'posts_per_page'    =>  4,
    );

    $the_query = new WP_Query($qargs);
    ?>

    <?php if ($the_query->have_posts()): ?>
        <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
            <div class="col-xs-12 col-md-3">

                <div class="item d-flex mb-3">

                    <?php the_post_thumbnail('full', ['class' => 'img-fluid zoom']); ?>

                    <div class="item-title">
                        <div class="content">
                            <a href="<?php the_permalink(); ?>">
                                <strong><?php the_title(); ?></strong>
                                <?php the_excerpt(); ?>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>

</div>