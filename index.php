<?php get_header(); ?>

<div class="container bg-light" style="margin-top: 90px;">

    <div class="content-page">

        <div class="row">
            <div class="col-12">

                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid pb-5')); ?>
                        <div class="container">
<!--                            <div class="content-body">-->
                                <?php the_content(); ?>
<!--                            </div>-->
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </div>
        </div>

    </div>

</div>

<?php get_footer() ?>