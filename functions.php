<?php

//Compatibilidad con Nav de Bootstrap
require_once('wp-bootstrap-navwalkers.php');

//Register the Menu
add_theme_support( 'nav-menus' );

if ( function_exists( 'register_nav_menus' ) ) {
    register_nav_menus(
        array(
            'main'      =>  'Main',
            'footer'    =>  'Footer'
        )
    );
}

//Habilitar thumbnails
add_theme_support('post-thumbnails');

?>