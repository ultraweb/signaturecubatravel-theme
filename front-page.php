<?php get_header(); ?>

    <?php echo do_shortcode('[metaslider title="Banner"]'); ?>

    <?php get_search_form(); ?>

    <div class="container bg-light">

        <div class="content-page">

            <!-- First Slider -->
            <?php get_template_part('content', 'slider-home'); ?>

            <!-- Itineraries Columns -->
            <?php get_template_part('content', 'itineraries'); ?>

            <!-- Destinations Columns -->
            <?php get_template_part('content', 'destinations'); ?>

            <!-- Form Contact -->
            <?php get_template_part('content', 'contact'); ?>

        </div>

    </div>

<?php get_footer() ?>