<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( bloginfo('title').'| ', true, 'left'); ?></title>

    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/js/bower_components/owlCarousel/assets/owl.carousel.min.css">
<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/css/animate.css/animate.css">
    <link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<?php wp_head(); ?>
</head>

<body>

<!-- Header Home -->
<div class="container-fluid bg-header bg-header-home">

    <nav class="navbar" role="navigation">
        <a class="navbar-brand" href="<?= home_url(); ?>"><img src="<?= get_stylesheet_directory_uri(); ?>/images/logo.png"></a>

        <ul class="nav justify-content-end social-links">
            <li class="nav-item">
                <a class="nav-link" href="https://g.page/signature-cuba-travel?share" target="_blank"><i class="mdi mdi-google-plus mdi-24px"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.facebook.com/SignatureCubaTravel/" target="_blank"><i class="mdi mdi-facebook mdi-24px"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://instagram.com/signaturecubatravel?igshid=13fctxdrh4nta" target="_blank"><i class="mdi mdi-instagram mdi-24px"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.youtube.com/channel/UCZm_syxUMxTqrxlybUtFHuw" target="_blank"><i class="mdi mdi-youtube-play mdi-24px"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://api.whatsapp.com/send?phone=15616293520" target="_blank" style="font-size: 24px;">(561) 629 3520</a>
            </li>
        </ul>

    </nav>

    <div class="container header-center-menu">
        <?php echo do_shortcode('[smartslider3 slider=1]'); ?>

        <nav class="navbar navbar-expand-md" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php
                wp_nav_menu( array(
                    'theme_location'    => 'main',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'bs-example-navbar-collapse-1',
                    'menu_class'        => 'nav nav-fill justify-content-center nav-home-center',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker(),
                ) );
                ?>
            </div>
        </nav>
    </div>

</div>




