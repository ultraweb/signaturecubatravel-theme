<div class="row">
    <div class="col-12 content-title text-center">
        <div class="divider">
            <span></span>
            <h3>Contact Us</h3>
            <span></span>
        </div>
    </div>
</div>

<div class="container contact-form pb-5">

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php echo do_shortcode('[contact-form-7 id="93" title="Contact form"]'); ?>
        </div>
    </div>

</div>
