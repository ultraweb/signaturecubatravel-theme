<div class="row">
    <div class="col-12 content-title text-center mt-5">
        <h2 class="text-uppercase mb-0 mt-5">Signature Cuba Travel</h2>
        <h4 class="text-uppercase small">The way to book your gateway</h4>
    </div>
</div>

<div class="row">

    <div class="col-12 pl-5 pr-5">
        <?php echo do_shortcode("[metaslider id=88]"); ?>
    </div>

</div>