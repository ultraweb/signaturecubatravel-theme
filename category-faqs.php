<?php get_header(); ?>

<?php
$qargs = array(
    'post_type'         =>  'post',
    'category_name'     =>  'faqs',
    'post_status'       =>  'publish',
    'order'             =>  'ASC',
);

$the_query = new WP_Query($qargs);
?>

    <div class="container content-page" style="margin-top: 90px;">

        <div class="row">
            <img src="<?= get_stylesheet_directory_uri(); ?>/images/faqs_bg.png" class="img-fluid" alt="Faqs">
        </div>

        <div class="content-faqs">

            <div class="content-body">
                <div class="accordion" id="accordionFaqs">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <div class="card shadow-sm p-0 bg-white rounded">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="#faq-<?php the_ID(); ?>" aria-expanded="true" aria-controls="faq-<?php the_ID(); ?>">
                                        <?php the_title(); ?>
                                        <span><i class="mdi mdi-chevron-down mdi-36px"></i></span>
                                    </button>
                                </h2>
                            </div>

                            <div id="faq-<?php the_ID(); ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionFaqs">
                                <div class="card-body">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
            </div>

        </div>

        <!-- Form Contact -->
        <?php get_template_part('content', 'contact'); ?>

    </div>

<?php get_footer() ?>