<div class="container">
    <div class="row">
        <div class="col-12 d-flex justify-content-center">

            <form id="searchform" action="<?php bloginfo('home'); ?>" method="get" class="searchform shadow">
                <div class="form-group">

                    <div class="input-group mb-2">
                        <input id="s" class="form-control" type="text" name="s" size="30" value="<?php echo $search_text; ?>" />
                        <div class="input-group-prepend">
                            <button type="submit" class="btn"><i class="mdi mdi-magnify mdi-36px"></i></button>
                        </div>
                    </div>
                </div>
                <input id="searchsubmit" type="hidden" />
            </form>

        </div>
    </div>
</div>